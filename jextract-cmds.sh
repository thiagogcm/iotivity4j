jextract \
    --missing-symbols exclude \
    /usr/include/signal.h -t org.unix.signal -o signal.jar

jextract \
    --missing-symbols exclude \
    -L /usr/lib -l pthread --record-library-path \
    /usr/include/pthread.h -t org.unix.pthread -o pthread.jar

jextract \
    --missing-symbols exclude \                                              
    /usr/include/unistd.h -t org.unix.unistd -o unistd.jar

jextract \
    --missing-symbols exclude \
    -I /usr/local/include/iotivity-constrained/ \
    -L /usr/local/lib -l iotivity-constrained-client-server --record-library-path \
    /usr/local/include/iotivity-constrained/oc_api.h -t org.iotivity -o iotivity.jar




LD_LIBRARY_PATH=/usr/lib:/usr/lib/x86_64-linux-gnu:/usr/local/lib

$JAVA_HOME/bin/jextract \
    -I /usr/local/include/iotivity-lite \
    -l iotivity-lite-client-server \
    -t tech.nthings.iotivity4j.binding.generated \
    -d tech.nthings.iotivity4j.binding.generated \
    --source \
    /usr/local/include/iotivity-lite/oc_api.h
