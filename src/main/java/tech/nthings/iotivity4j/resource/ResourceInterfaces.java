package tech.nthings.iotivity4j.resource;

//import org.iotivity.oc_ri_lib;

import java.util.EnumSet;

import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public enum ResourceInterfaces {
	BASELINE(OC_IF_BASELINE()),
	LL(OC_IF_LL()),
	B(OC_IF_B()),
	R(OC_IF_R()),
	RW(OC_IF_RW()),
	A(OC_IF_A()),
	S(OC_IF_S());

    private int value;

    ResourceInterfaces(final int v) {
      value = v;
    }
 
    public int getResourceInterface() {
      return value;
    }

    public static EnumSet<ResourceInterfaces> fromInt(int ifaceMask) {
        var ifs = EnumSet.noneOf(ResourceInterfaces.class);
        
        for (var i : values()) {
            if ((ifaceMask & i.getResourceInterface()) != 0) {
                ifs.add(i);
            }
        }
        
        return ifs;
    }
}
