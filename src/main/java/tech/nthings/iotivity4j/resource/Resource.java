package tech.nthings.iotivity4j.resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.nthings.iotivity4j.resource.endpoint.Endpoint;

import java.util.EnumSet;
import java.util.List;

public final class Resource {
	private static final Logger logger = LoggerFactory.getLogger(Resource.class);

    private final String anchor;
	private final String href;
    private final List<String> resourceTypes;
    private final EnumSet<ResourceInterfaces> ifaceMask;
    private final Endpoint endpoint;
    private final EnumSet<ResourceProperties> bmMask;

	public Resource(
			String anchor,
			String href,
			List<String> resourceTypes,
			EnumSet<ResourceInterfaces> ifaceMask,
			Endpoint endpoint,
			EnumSet<ResourceProperties> bmMask) {
    	this.anchor = anchor;
		this.href = href;
        this.resourceTypes = resourceTypes;
        this.ifaceMask = ifaceMask;
        this.endpoint = endpoint;
        this.bmMask = bmMask;
    }

	public String getAnchor() {
		return this.anchor;
	}

	public String getHref() {
		return this.href;
	}

	public List<String> getResourceTypes() {
		return this.resourceTypes;
	}

	public Endpoint getEndpoint() {
		return this.endpoint;
	}

	public EnumSet<ResourceInterfaces> getIfaceMask() {
		return this.ifaceMask;
	}

	public EnumSet<ResourceProperties> getBmMask() {
		return this.bmMask;
	}

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			logger.error("Failed to convert to JSON", e);
		}
		return null;
	}
}
