package tech.nthings.iotivity4j.resource;

//import org.iotivity.oc_ri_lib;

import java.util.EnumSet;

import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public enum ResourceProperties {
    DISCOVERABLE(OC_DISCOVERABLE()),
    OBSERVABLE(OC_OBSERVABLE()),
    SECURE(OC_SECURE()),
    PERIODIC(OC_PERIODIC());

    private int value;

    ResourceProperties(final int v) {
      value = v;
    }
 
    public int getResourceProperty() {
      return value;
    }

    public static EnumSet<ResourceProperties> fromInt(int bmMask) {
        var rps = EnumSet.noneOf(ResourceProperties.class);
        
        for (var rp : values()) {
            if ((bmMask & rp.getResourceProperty()) != 0) {
                rps.add(rp);
            }
        }
        
        return rps;
    }
}
