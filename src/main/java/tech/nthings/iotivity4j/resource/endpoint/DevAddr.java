package tech.nthings.iotivity4j.resource.endpoint;

public interface DevAddr {
	
	String getAddress();

	int getPort();
	
	byte getScope();
}
