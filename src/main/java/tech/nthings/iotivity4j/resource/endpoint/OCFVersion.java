package tech.nthings.iotivity4j.resource.endpoint;

//import org.iotivity.oc_endpoint_lib;
import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public enum OCFVersion {
	OCF_VER_1_0_0(OCF_VER_1_0_0()),
	OIC_VER_1_1_0(OIC_VER_1_1_0());
	
	private int version;

    OCFVersion(final int v) {
        version = v;
    }

    public static OCFVersion fromInt(int version) {
        if (version == OCF_VER_1_0_0()) {
            return OCF_VER_1_0_0;
        } else if (version == OIC_VER_1_1_0()) {
            return OIC_VER_1_1_0;
        }
        
        return null;
    }

    public int getOCFVersion() {
        return version;
    }
}
