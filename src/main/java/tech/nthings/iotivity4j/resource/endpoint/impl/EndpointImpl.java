package tech.nthings.iotivity4j.resource.endpoint.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.nthings.iotivity4j.resource.endpoint.DevAddr;
import tech.nthings.iotivity4j.resource.endpoint.Endpoint;
import tech.nthings.iotivity4j.resource.endpoint.OCFVersion;
import tech.nthings.iotivity4j.resource.endpoint.TransportFlags;

import java.util.EnumSet;
import java.util.UUID;

public final class EndpointImpl implements Endpoint {
	private static final Logger logger = LoggerFactory.getLogger(EndpointImpl.class);
	
	private final DevAddr devAddr;
	private final UUID di;
	private final long device;
	private final EnumSet<TransportFlags> flags;
    private final int interfaceIndex;
	private final byte priority;
    private final OCFVersion version;
	
	EndpointImpl(DevAddr devAddr,
				 UUID di,
				 long device,
				 EnumSet<TransportFlags> flags,
				 int interfaceIndex,
				 byte priority,
				 OCFVersion version) {
		
		this.devAddr = devAddr;
        this.di = di;
		this.device = device;
		this.flags = flags;
        this.interfaceIndex = interfaceIndex;
		this.priority = priority;
        this.version = version;
	}

	@Override
	public DevAddr getDevAddr() {
		return this.devAddr;
	}

	@Override
	public UUID getDi() {
        return this.di;
	}

	@Override
	public long getDevice() {
		return this.device;
	}

	@Override
	public EnumSet<TransportFlags> getFlags() {
		return this.flags;
	}

	@Override
    public int getInterfaceIndex() {
        return this.interfaceIndex;
	}

	@Override
	public byte getPriority() {
		return this.priority;
	}

	@Override
    public OCFVersion getVersion() {
        return this.version;
    }

	@Override
	public String toString() {
		try {
			return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			logger.error("Failed to convert to JSON", e);
		}
		return null;
	}

	public static class EndpointBuilderImpl implements EndpointBuilder {
		
		private DevAddr innerDevAddr;
		private UUID innerDi;
		private long innerDevice;
		private EnumSet<TransportFlags> innerFlags;
        private int innerInterfaceIndex;
		private byte innerPriority;
        private OCFVersion innerVersion;
		
		public EndpointBuilderImpl() {
			// TODO: Provide default properties?
		}
		
		@Override
		public EndpointBuilder setDevAddr(DevAddr newDevAddr) {
			this.innerDevAddr = newDevAddr;
			return this;
		}

		@Override
		public EndpointBuilder setDi(UUID newDi) {
            this.innerDi = newDi;
			return this;
		}
		
		@Override
		public EndpointBuilder setDevice(long newDevice) {
			this.innerDevice = newDevice;
			return this;
		}

		@Override
		public EndpointBuilder setFlags(EnumSet<TransportFlags> newFlags) {
			this.innerFlags = newFlags;
			return this;
		}

		@Override
        public EndpointBuilder setInterfaceIndex(int newInterfaceIndex) {
            this.innerInterfaceIndex = newInterfaceIndex;
			return this;
		}

		@Override
		public EndpointBuilder setPriority(byte newPriority) {
			this.innerPriority = newPriority;
			return this;
		}

        @Override
        public EndpointBuilder setVersion(OCFVersion newVersion) {
            this.innerVersion = newVersion;
            return this;
        }

		@Override
		public Endpoint build() {
            return new EndpointImpl(innerDevAddr, innerDi, innerDevice, innerFlags, innerInterfaceIndex, innerPriority, innerVersion);
		}
	}
}
