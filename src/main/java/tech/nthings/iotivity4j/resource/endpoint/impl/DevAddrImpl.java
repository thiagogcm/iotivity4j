package tech.nthings.iotivity4j.resource.endpoint.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.nthings.iotivity4j.resource.endpoint.DevAddr;

public class DevAddrImpl implements DevAddr {
    private static final Logger logger = LoggerFactory.getLogger(DevAddrImpl.class);

    private final String address;
    private final int port;
    private final byte scope;

    public DevAddrImpl(String address, int port, byte scope) {
        super();
        this.address = address;
        this.port = port;
        this.scope = scope;
    }

    @Override
    public String getAddress() {
        return this.address;
    }

    @Override
    public int getPort() {
        return this.port;
    }

    @Override
    public byte getScope() {
        return this.scope;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert to JSON", e);
        }
        return null;
    }
}
