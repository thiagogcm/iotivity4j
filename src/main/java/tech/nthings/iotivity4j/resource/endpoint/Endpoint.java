package tech.nthings.iotivity4j.resource.endpoint;

import tech.nthings.iotivity4j.resource.endpoint.impl.EndpointImpl;

import java.util.EnumSet;
import java.util.UUID;

public interface Endpoint {

	DevAddr getDevAddr();

	UUID getDi();
	
	long getDevice();
	
	EnumSet<TransportFlags> getFlags();

	int getInterfaceIndex();
	
	byte getPriority();

	OCFVersion getVersion();
	
	static EndpointBuilder newEndpointBuilder() {
		return new EndpointImpl.EndpointBuilderImpl();
	}
	
	interface EndpointBuilder {
		
		EndpointBuilder setDevAddr(DevAddr devAddr);

		EndpointBuilder setDi(UUID di);
		
		EndpointBuilder setDevice(long device);
		
		EndpointBuilder setFlags(EnumSet<TransportFlags> flags);

		EndpointBuilder setInterfaceIndex(int interfaceIndex);
		
		EndpointBuilder setPriority(byte priority);

		EndpointBuilder setVersion(OCFVersion version);
		
		Endpoint build();
	}
}
