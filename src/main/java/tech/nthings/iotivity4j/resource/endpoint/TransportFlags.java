package tech.nthings.iotivity4j.resource.endpoint;

//import org.iotivity.oc_endpoint_lib;

import java.util.EnumSet;

import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public enum TransportFlags {
	DISCOVERY(DISCOVERY()),
	SECURED(SECURED()),
	IPV4(IPV4()),
	IPV6(IPV6()),
	TCP(TCP()),
	GATT(GATT()),
	MULTICAST(MULTICAST());

    private int value;

    TransportFlags(final int v) {
      value = v;
    }
 
    public int getTransportFlag() {
      return value;
    }

    public static EnumSet<TransportFlags> fromInt(int flagMask) {
        var ifs = EnumSet.noneOf(TransportFlags.class);
        
        for (var i : values()) {
            if ((flagMask & i.getTransportFlag()) != 0) {
                ifs.add(i);
            }
        }
        
        return ifs;
    }
}
