package tech.nthings.iotivity4j.discovery;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.discovery.impl.DiscoveryImpl;
import tech.nthings.iotivity4j.resource.Resource;

import java.util.function.Consumer;

public interface Discovery {

    void discover(String resourceType, Consumer<AsyncResult<Resource>> handler);

    Future<Resource> discover(String resourceType);

    void discoverAll(Consumer<AsyncResult<Resource>> handler);

    Future<Resource> discoverAll();

	static Discovery newDiscovery() {
		return new DiscoveryImpl();
	} 
}
