package tech.nthings.iotivity4j.discovery.impl;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.async.Promise;
import tech.nthings.iotivity4j.binding.api.discovery.OcDiscovery;
import tech.nthings.iotivity4j.binding.api.discovery.impl.OcDiscoveryImpl;
import tech.nthings.iotivity4j.binding.spi.OcBinding;
import tech.nthings.iotivity4j.discovery.Discovery;
import tech.nthings.iotivity4j.resource.Resource;

import java.util.function.Consumer;

public class DiscoveryImpl implements Discovery {
    private final OcDiscovery ocDiscovery;
    
    public DiscoveryImpl() {
        //ocDiscovery = OcBinding.of(OcDiscovery.class);
        ocDiscovery = new OcDiscoveryImpl();
    }

    @Override
    public void discover(String resourceType, Consumer<AsyncResult<Resource>> handler) {
        ocDiscovery.discover(resourceType, handler);
    }

    @Override
    public Future<Resource> discover(String resourceType) {
        var promise = Promise.<Resource>promise();
        discover(resourceType, promise);
        return promise.future();
    }

    @Override
    public void discoverAll(Consumer<AsyncResult<Resource>> handler) {
        ocDiscovery.discoverAll(handler);
    }

    @Override
    public Future<Resource> discoverAll() {
        var promise = Promise.<Resource>promise();
        discoverAll(promise);
        return promise.future();
    }
}
