package tech.nthings.iotivity4j.client;

import tech.nthings.iotivity4j.client.impl.ClientRequestImpl;
import tech.nthings.iotivity4j.resource.endpoint.Endpoint;

public interface ClientRequest {
	enum Method {
		GET,
		POST,
		PUT,
		DELETE,
		OBSERVE
	}
	
	Method getMethod();
	
	Endpoint getEndpoint();
	
	String getUri();
	
	String getQuery();
	
	QualityOfService getQualityOfService();
	
	static ClientRequestBuilder newClientRequestBuilder() {
		return new ClientRequestImpl.ClientRequestBuilderImpl();
	}
	
	interface ClientRequestBuilder {
		
		ClientRequestBuilder setEndpoint(Endpoint endpoint);
		
		ClientRequestBuilder setUri(String uri);
		
		ClientRequestBuilder setQuery(String query);
		
		ClientRequestBuilder setMethod(Method method);
		
		ClientRequestBuilder setQualityOfService(QualityOfService qos);
		
		ClientRequest build();
	}
}
