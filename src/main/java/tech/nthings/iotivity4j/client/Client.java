package tech.nthings.iotivity4j.client;

import java.util.function.Consumer;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.client.impl.ClientImpl;

public interface Client {
	
	static Client newClient() {
		return new ClientImpl();
	}
	
	void send(ClientRequest request, Consumer<AsyncResult<ClientResponse>> handler);
	
	Future<ClientResponse> send(ClientRequest request);
}
