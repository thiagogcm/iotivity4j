package tech.nthings.iotivity4j.client.impl;

import java.util.function.Consumer;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.binding.api.client.OcClient;
import tech.nthings.iotivity4j.binding.api.client.impl.OcClientImpl;
import tech.nthings.iotivity4j.binding.spi.OcBinding;
import tech.nthings.iotivity4j.client.Client;
import tech.nthings.iotivity4j.client.ClientRequest;
import tech.nthings.iotivity4j.client.ClientResponse;

public class ClientImpl implements Client {
	
	private final OcClient ocClient;

	public ClientImpl() {
		//ocClient = OcBinding.of(OcClient.class);
		ocClient = new OcClientImpl();
	}
	
	@Override
	public void send(ClientRequest request, Consumer<AsyncResult<ClientResponse>> handler) {
		ocClient.send(request, handler);
	}

	@Override
	public Future<ClientResponse> send(ClientRequest request) {
		throw new UnsupportedOperationException();
	}
}
