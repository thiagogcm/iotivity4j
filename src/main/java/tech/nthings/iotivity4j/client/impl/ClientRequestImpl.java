package tech.nthings.iotivity4j.client.impl;

import tech.nthings.iotivity4j.client.ClientRequest;
import tech.nthings.iotivity4j.client.QualityOfService;
import tech.nthings.iotivity4j.resource.endpoint.Endpoint;

public class ClientRequestImpl implements ClientRequest {

	private final Method method;
	private final Endpoint endpoint;
	private final String uri;
	private final String query;
	private final QualityOfService qos;
	
	ClientRequestImpl(Method method, Endpoint endpoint, String uri, String query, QualityOfService qos) {
		this.method = method;
		this.endpoint = endpoint;
		this.uri = uri;
		this.query = query;
		this.qos = qos;
	}

	@Override
	public Method getMethod() {
		return this.method;
	}

	@Override
	public Endpoint getEndpoint() {
		return this.endpoint;
	}

	@Override
	public String getUri() {
		return this.uri;
	}

	@Override
	public String getQuery() {
		return this.query;
	}
	
	@Override
	public QualityOfService getQualityOfService() {
		return this.qos;
	}
	
	public static class ClientRequestBuilderImpl implements ClientRequest.ClientRequestBuilder {
		
		private Method innerMethod;
		private Endpoint innerEndpoint;
		private String innerUri;
		private String innerQuery;
		private QualityOfService innerQos;

		@Override
		public ClientRequestBuilder setEndpoint(Endpoint newEndpoint) {
			this.innerEndpoint = newEndpoint;
			return this;
		}

		@Override
		public ClientRequestBuilder setUri(String newUri) {
			this.innerUri = newUri;
			return this;
		}

		@Override
		public ClientRequestBuilder setQuery(String newQuery) {
			this.innerQuery = newQuery;
			return null;
		}

		@Override
		public ClientRequestBuilder setMethod(Method newMethod) {
			this.innerMethod = newMethod;
			return this;
		}
		
		@Override
		public ClientRequestBuilder setQualityOfService(QualityOfService newQos) {
			this.innerQos = newQos;
			return this;
		}

		@Override
		public ClientRequest build() {
			return new ClientRequestImpl(
					this.innerMethod,
					this.innerEndpoint,
					this.innerUri,
					this.innerQuery,
					this.innerQos);
		}
	}
}
