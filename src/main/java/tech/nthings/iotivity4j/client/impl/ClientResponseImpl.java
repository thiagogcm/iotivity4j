package tech.nthings.iotivity4j.client.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.nthings.iotivity4j.client.ClientResponse;
import tech.nthings.iotivity4j.resource.endpoint.Endpoint;

import java.util.Map;

public class ClientResponseImpl implements ClientResponse {
	private static final Logger logger = LoggerFactory.getLogger(ClientResponseImpl.class);

	private final Map<String, Object> payload;
	private final Endpoint endpoint;
	private final int observeOption;
	private final Status status;
	
	public ClientResponseImpl(Map<String, Object> payload, Endpoint endpoint, int observeOption, Status status) {
		this.payload = payload;
		this.endpoint = endpoint;
		this.observeOption = observeOption;
		this.status = status;
	}

	@Override
	public Map<String, Object> getPayload() {
		return this.payload;
	}

	@Override
	public Endpoint getEndpoint() {
		return this.endpoint;
	}

	@Override
	public int getObserveOption() {
		return this.observeOption;
	}

	@Override
	public Status getStatus() {
		return this.status;
	}
	
	@Override
	public String toString() {
		try {
			return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			logger.error("Failed to convert to JSON", e);
		}
		return null;
	}

	public static class ClientResponseBuilderImpl implements ClientResponseBuilder {
		
		private Map<String, Object> innerPayload;
		private Endpoint innerEndpoint;
		private int innerObserveOption;
		private Status innerStatus;
		
		@Override
		public ClientResponseBuilder setPayload(Map<String, Object> newPayload) {
			this.innerPayload = newPayload;
			return this;
		}

		@Override
		public ClientResponseBuilder setEndpoint(Endpoint newEndpoint) {
			this.innerEndpoint = newEndpoint;
			return this;
		}

		@Override
		public ClientResponseBuilder setObserveOption(int newObserveOption) {
			this.innerObserveOption = newObserveOption;
			return this;
		}

		@Override
		public ClientResponseBuilder setStatus(Status newStatus) {
			this.innerStatus = newStatus;
			return this;
		}
		
		@Override
		public ClientResponse build() {	
			return new ClientResponseImpl(
					this.innerPayload,
					this.innerEndpoint,
					this.innerObserveOption,
					this.innerStatus);
		}
	}
}
