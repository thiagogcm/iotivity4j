package tech.nthings.iotivity4j.client;

//import org.iotivity.oc_ri_lib;
import tech.nthings.iotivity4j.client.impl.ClientResponseImpl;
import tech.nthings.iotivity4j.resource.endpoint.Endpoint;

import java.util.Map;

import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public interface ClientResponse {

    enum Status {
		OK(OC_STATUS_OK()),
	    CREATED(OC_STATUS_CREATED()),
	    CHANGED(OC_STATUS_CHANGED()),
	    DELETED(OC_STATUS_DELETED()),
	    NOT_MODIFIED(OC_STATUS_NOT_MODIFIED()),
	    BAD_REQUEST(OC_STATUS_BAD_REQUEST()),
	    UNAUTHORIZED(OC_STATUS_UNAUTHORIZED()),
	    BAD_OPTION(OC_STATUS_BAD_OPTION()),
	    FORBIDDEN(OC_STATUS_FORBIDDEN()),
		NOT_FOUND(OC_STATUS_NOT_FOUND()),
		METHOD_NOT_ALLOWED(OC_STATUS_METHOD_NOT_ALLOWED()),
		NOT_ACCEPTABLE(OC_STATUS_NOT_ACCEPTABLE()),
		REQUEST_ENTITY_TOO_LARGE(OC_STATUS_REQUEST_ENTITY_TOO_LARGE()),
		UNSUPPORTED_MEDIA_TYPE(OC_STATUS_UNSUPPORTED_MEDIA_TYPE()),
		INTERNAL_SERVER_ERROR(OC_STATUS_INTERNAL_SERVER_ERROR()),
		NOT_IMPLEMENTED(OC_STATUS_NOT_IMPLEMENTED()),
		BAD_GATEWAY(OC_STATUS_BAD_GATEWAY()),
		SERVICE_UNAVAILABLE(OC_STATUS_SERVICE_UNAVAILABLE()),
		GATEWAY_TIMEOUT(OC_STATUS_GATEWAY_TIMEOUT()),
		PROXYING_NOT_SUPPORTED(OC_STATUS_PROXYING_NOT_SUPPORTED());
		// TODO: Map more...
		
	    private int status;
	    private static Status[] cacheValues = Status.values();
	 
	    private Status(final int s) {
	      status = s;
	    }
	 
	    public int getStatus() {
	      return status;
	    }

	    public static Status from(int i) {
	        return cacheValues[i];
	    }
	}
	
	Map<String, Object> getPayload();
	
	Endpoint getEndpoint();
	
	int getObserveOption();
	
	Status getStatus(); // code field
	
	static ClientResponseBuilder newClientResponseBuilder() {
		return new ClientResponseImpl.ClientResponseBuilderImpl();
	}
	
	interface ClientResponseBuilder {
		
		ClientResponseBuilder setPayload(Map<String, Object> payload);
		
		ClientResponseBuilder setEndpoint(Endpoint endpoint);
		
		ClientResponseBuilder setObserveOption(int observeOption);
		
		ClientResponseBuilder setStatus(Status status);
		
		ClientResponse build();
	}
}
