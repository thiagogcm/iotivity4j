package tech.nthings.iotivity4j.client;

//import org.iotivity.oc_client_state_lib;

import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public enum QualityOfService {
	HIGH(HIGH_QOS()),
	LOW(LOW_QOS());
	
	private int qos;
	 
    private QualityOfService(final int v) {
      qos = v;
    }
 
    public int getQos() {
      return qos;
    }
}
