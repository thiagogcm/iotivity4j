package tech.nthings.iotivity4j.binding.spi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.stream.Stream;

public interface OcBinding {
    Logger logger = LoggerFactory.getLogger(OcBinding.class);
/*     Map<Class<?>, OcBinding> bindings = new HashMap<>();

    static <T extends OcBinding> T of(Class<T> clazz) {
        if (bindings.isEmpty()) {
            ServiceLoader.load(OcBinding.class).forEach(bind ->
                    Stream.of(bind.getClass().getInterfaces()).forEach(i -> {
                        try {
                            bindings.put(i, bind.getClass().getDeclaredConstructor().newInstance());
                        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                            logger.error("Failed to instantiate binding {}", clazz.getName(), e);
                        }
                    }));
        }

        return clazz.cast(bindings.get(clazz));
    }
 */
    void cleanup();
/* 
    static void cleanupAll() {
        bindings.values().forEach(bind -> {
        	logger.debug("Cleaning up {}", bind.getClass().getSimpleName());
            bind.cleanup();
        });
    } */
}
