package tech.nthings.iotivity4j.binding.storage;

import java.nio.file.Path;

import tech.nthings.iotivity4j.binding.spi.OcBinding;

public interface OcStorage extends OcBinding {
    void config(Path path);
}
