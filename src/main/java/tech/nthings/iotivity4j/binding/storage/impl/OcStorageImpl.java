package tech.nthings.iotivity4j.binding.storage.impl;

import java.nio.file.Path;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jdk.incubator.foreign.NativeScope;
import tech.nthings.iotivity4j.binding.storage.OcStorage;

import static jdk.incubator.foreign.CLinker.*;
import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.oc_storage_config;

public final class OcStorageImpl implements OcStorage {
    private static final Logger logger = LoggerFactory.getLogger(OcStorageImpl.class);

    private final NativeScope scope;

    public OcStorageImpl() {
        this.scope = NativeScope.unboundedScope();
    }

    @Override
    public void config(Path path) {
        logger.debug("Setting SVR dir: {}", path);
        Objects.requireNonNull(path);
        
        oc_storage_config(toCString(path.toString(), scope));
    }

    @Override
    public void cleanup() {
        scope.close();
    }
}
