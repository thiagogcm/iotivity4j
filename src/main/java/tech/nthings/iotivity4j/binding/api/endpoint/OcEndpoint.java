package tech.nthings.iotivity4j.binding.api.endpoint;

import jdk.incubator.foreign.MemoryAddress;
import jdk.incubator.foreign.NativeScope;
import tech.nthings.iotivity4j.binding.helpers.OcHelpers;
import tech.nthings.iotivity4j.resource.endpoint.Endpoint;
import tech.nthings.iotivity4j.resource.endpoint.OCFVersion;
import tech.nthings.iotivity4j.resource.endpoint.TransportFlags;
import tech.nthings.iotivity4j.resource.endpoint.impl.DevAddrImpl;

public interface OcEndpoint {
	
 	static Endpoint fromNative(MemoryAddress endpoint) {
		/*try (var scope = oc_endpoint_lib.scope().fork()) {
			// oc_dev_addr conversion
			var nativeDevAddr = endpoint.addr$get();
			var ocStr = scope.allocateStruct(oc_mmem_h.oc_mmem.class);

			oc_endpoint_lib.oc_endpoint_to_string(endpoint.ptr(), ocStr.ptr());

			var address = Pointer.toString(ocStr.ptr$get().cast(NativeTypes.CHAR));
			var port = Short.toUnsignedInt(nativeDevAddr.ipv6$get().port$get());
			var ipv6Scope = nativeDevAddr.ipv6$get().scope$get();
			var devAddr = new DevAddrImpl(address, port, ipv6Scope);

			// rest of endpoint configuration conversion
			var device = endpoint.device$get();
            var di = OcHelpers.ocUUIDToUUID(endpoint.di$get());
			var flags = TransportFlags.fromInt(endpoint.flags$get());
			var interfaceIndex = endpoint.interface_index$get();
			var priority = endpoint.priority$get();
			var version = OCFVersion.fromInt(endpoint.version$get());

			return Endpoint.newEndpointBuilder()
					.setDevAddr(devAddr)
					.setDevice(device)
					.setDi(di)
					.setFlags(flags)
					.setInterfaceIndex(interfaceIndex)
					.setPriority(priority)
					.setVersion(version)
					.build();
		}
		*/
		return null;
	}

	static MemoryAddress toNative(Endpoint endpoint, NativeScope scope) {
		/* var ep = scope.allocateStruct(oc_endpoint_t.class);

		// native dev_addr conversion
		var ipv6 = scope.allocateStruct(oc_endpoint_h.oc_ipv6_addr_t.class);
		var ocStr = scope.allocate(LayoutType.ofStruct(oc_mmem_h.oc_mmem.class));
		var address = scope.allocateCString(endpoint.getDevAddr().getAddress());
		var da = scope.allocateStruct(oc_endpoint_h.dev_addr.class);

		oc_helpers_lib._oc_new_string(ocStr, address, endpoint.getDevAddr().getAddress().length());
		oc_endpoint_lib.oc_string_to_endpoint(ocStr, ep.ptr(), Pointer.ofNull());

		ipv6.address$set(ep.ptr().get().addr$get().ipv6$get().address$get());
		ipv6.port$set(Integer.valueOf(endpoint.getDevAddr().getPort()).shortValue());
		ipv6.scope$set(endpoint.getDevAddr().getScope());

		da.ipv6$set(ipv6);

		// rest of endpoint configuration conversion
		ep.addr$set(da);
		ep.device$set(endpoint.getDevice());
        ep.di$set(OcHelpers.UUIDToOcUUID(endpoint.getDi(), scope));
		ep.flags$set(OcHelpers.encode(endpoint.getFlags()));
		ep.interface_index$set(endpoint.getInterfaceIndex());
		ep.priority$set(endpoint.getPriority());
		ep.version$set(endpoint.getVersion().getOCFVersion());

		return ep; */
		return null;
	}
	
}
