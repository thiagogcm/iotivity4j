package tech.nthings.iotivity4j.binding.api.discovery;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.binding.spi.OcBinding;
import tech.nthings.iotivity4j.resource.Resource;

import java.util.function.Consumer;

public interface OcDiscovery extends OcBinding {
    void discover(String resourceType, Consumer<AsyncResult<Resource>> handler);

    void discoverAll(Consumer<AsyncResult<Resource>> handler);
}
