package tech.nthings.iotivity4j.binding.api.platform;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.binding.spi.OcBinding;
import tech.nthings.iotivity4j.platform.DeviceInfo;

import java.util.List;
import java.util.function.Consumer;

public interface OcPlatformManager extends OcBinding {

    void init(String name, List<DeviceInfo> devices, Consumer<AsyncResult<Void>> handler);

    void shutdown();
}
