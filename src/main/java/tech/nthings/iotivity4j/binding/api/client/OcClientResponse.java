package tech.nthings.iotivity4j.binding.api.client;


import jdk.incubator.foreign.MemoryAddress;
import tech.nthings.iotivity4j.binding.api.endpoint.OcEndpoint;
import tech.nthings.iotivity4j.client.ClientResponse;
import tech.nthings.iotivity4j.client.ClientResponse.Status;

public interface OcClientResponse {
	
	static ClientResponse fromNative(MemoryAddress ocClientResponse) {
		/* var endpoint = OcEndpoint.fromNative(ocClientResponse.endpoint$get().get());
		var observeOption = ocClientResponse.observe_option$get();
		var status = Status.from(ocClientResponse.code$get());
		var payload = OcResourceRepresentation.fromNative(ocClientResponse.payload$get());

		return ClientResponse.newClientResponseBuilder()
			.setEndpoint(endpoint)
			.setObserveOption(observeOption)
			.setStatus(status)
			.setPayload(payload)
			.build(); */
		
		return null;
	}
	
	static MemoryAddress toNative(ClientResponse clientResponse) {
		throw new UnsupportedOperationException();
	}
}
