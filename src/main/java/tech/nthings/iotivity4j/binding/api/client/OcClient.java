package tech.nthings.iotivity4j.binding.api.client;

import java.util.function.Consumer;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.binding.spi.OcBinding;
import tech.nthings.iotivity4j.client.ClientRequest;
import tech.nthings.iotivity4j.client.ClientResponse;

public interface OcClient extends OcBinding {
	void send(ClientRequest request, Consumer<AsyncResult<ClientResponse>> handler);
}
