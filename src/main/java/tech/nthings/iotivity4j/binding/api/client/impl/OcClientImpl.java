package tech.nthings.iotivity4j.binding.api.client.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.binding.api.client.OcClient;
import tech.nthings.iotivity4j.binding.api.client.OcClientResponse;
import tech.nthings.iotivity4j.binding.api.endpoint.OcEndpoint;
import tech.nthings.iotivity4j.client.ClientRequest;
import tech.nthings.iotivity4j.client.ClientResponse;

import java.util.function.Consumer;

import jdk.incubator.foreign.NativeScope;

import static jdk.incubator.foreign.CLinker.*;
import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public class OcClientImpl implements OcClient {
	private static final Logger logger = LoggerFactory.getLogger(OcClientImpl.class);

	private final NativeScope scope;
	
	public OcClientImpl() {
		this.scope = NativeScope.unboundedScope();
	}
	
	@Override
	public void send(ClientRequest request, Consumer<AsyncResult<ClientResponse>> handler) {
/* 		var uri = toCString(request.getUri(), scope);
        var endpoint = OcEndpoint.toNative(request.getEndpoint(), scope);
		
		// TODO: Handle query param
		var query = request.getQuery() != null ? toCString(request.getUri(), scope) : Pointer.ofNull();
		
		var callback = oc_resource_set_request_handler$callback.allocate(requestHandler(handler), scope);
		var qos = request.getQualityOfService().getQos();
		
		switch (request.getMethod()) {
			case GET: oc_api_lib.oc_do_get(uri, endpoint.ptr(), Pointer.ofNull(), callback, qos, Pointer.ofNull());
			break;
			
			default: throw new UnsupportedOperationException();
		} */
		
		// TODO: Handle request return
//		if (success) {
//			handler.accept(Future.succeededFuture());
//        } else {
//        	handler.accept(Future.failedFuture("Failed to execute query"));
//        }
	}

/* 	private oc_resource_set_request_handler$callback requestHandler(Consumer<AsyncResult<ClientResponse>> handler) {
		return (response) -> {
			handler.accept(Future.succeededFuture(OcClientResponse.fromNative(response.get())));
		};
	} */
	
	@Override
	public void cleanup() {
		scope.close();
	}
}
