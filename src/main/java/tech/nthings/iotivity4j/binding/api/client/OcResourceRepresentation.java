package tech.nthings.iotivity4j.binding.api.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import jdk.incubator.foreign.MemoryAddress;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public interface OcResourceRepresentation {
	static Map<String, Object> fromNative(MemoryAddress ocRepPtr) {
		/* TypeReference<HashMap<String, Object>> typeRef = new TypeReference<>() {
		};
		Map<String, Object> result = null;

		if (!ocRepPtr.isNull()) {
			try (var scope = oc_rep_lib.scope().fork()) {
				var ocRep = ocRepPtr.get();
				var jsonSize = oc_rep_lib.oc_rep_to_json(ocRep.ptr(), Pointer.ofNull(), 0, false);
				var jsonPtr = scope.allocate(NativeTypes.CHAR, jsonSize + 1);
				oc_rep_lib.oc_rep_to_json(ocRep.ptr(), jsonPtr, jsonSize + 1, false);

				var json = Pointer.toString(jsonPtr);

				result = new ObjectMapper().readValue(json, typeRef);
			} catch (IOException e) {
				throw new RuntimeException(); // TODO: Define better exceptions
			}
		}

		 return result;
		 */
		return null;
	}
	
	static MemoryAddress toNative(Map<String, Object> resourceRepresentation) {
		throw new UnsupportedOperationException();
	}
}
