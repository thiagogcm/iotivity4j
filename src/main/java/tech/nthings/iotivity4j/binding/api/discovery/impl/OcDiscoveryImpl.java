package tech.nthings.iotivity4j.binding.api.discovery.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jdk.incubator.foreign.MemoryAddress;
import jdk.incubator.foreign.MemorySegment;
import jdk.incubator.foreign.NativeScope;
import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.binding.api.discovery.OcDiscovery;
import tech.nthings.iotivity4j.binding.api.endpoint.OcEndpoint;
import tech.nthings.iotivity4j.binding.helpers.OcHelpers;
import tech.nthings.iotivity4j.resource.Resource;
import tech.nthings.iotivity4j.resource.ResourceInterfaces;
import tech.nthings.iotivity4j.resource.ResourceProperties;

import java.util.function.Consumer;

import static jdk.incubator.foreign.CLinker.*;
import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public final class OcDiscoveryImpl implements OcDiscovery {
    private static final Logger logger = LoggerFactory.getLogger(OcDiscoveryImpl.class);

    private final NativeScope scope;

    /* TODO: Implement Timeout and ResourceType filter on discovery execution */

    public OcDiscoveryImpl() {
        this.scope = NativeScope.unboundedScope();
    }

    @Override
    public void discover(String resourceType, Consumer<AsyncResult<Resource>> handler) {
        var success = oc_do_ip_discovery(
                toCString(resourceType, scope),
                oc_do_ip_discovery$handler.allocate(discoveryHandler(handler), scope),
                NULL());

        handleSuccess(handler, success);
    }

    @Override
    public void discoverAll(Consumer<AsyncResult<Resource>> handler) {
        // TODO: Aggregate all resources from a device on a List
        var success = oc_do_ip_discovery_all(
            oc_do_ip_discovery_all$handler.allocate(discoveryAllHandler(handler), scope),
                NULL());

        handleSuccess(handler, success);
    }

    @Override
    public void cleanup() {
        scope.close();
    }

    private oc_do_ip_discovery$handler discoveryHandler(Consumer<AsyncResult<Resource>> handler) {
        return (ocAnchor, ocUri, ocTypes, ocIfaceMask, ocEndpoint, ocBmMask, ocUserData) -> {
            var discoveryResponse = createResource(ocAnchor, ocUri, ocTypes, ocIfaceMask, ocEndpoint, ocBmMask);
            handler.accept(Future.succeededFuture(discoveryResponse));

            return OC_CONTINUE_DISCOVERY();
        };
    }

    private oc_do_ip_discovery_all$handler discoveryAllHandler(Consumer<AsyncResult<Resource>> handler) {
        return (ocAnchor, ocUri, ocTypes, ocIfaceMask, ocEndpoint, ocBmMask, more, ocUserData) -> {
            var discoveryResponse = createResource(ocAnchor, ocUri, ocTypes, ocIfaceMask, ocEndpoint, ocBmMask);
            handler.accept(Future.succeededFuture(discoveryResponse));

            if (more == 0) {
                logger.debug("Stopping discovery");
                return OC_STOP_DISCOVERY();
            }

            return OC_CONTINUE_DISCOVERY();
        };
    }

    private Resource createResource(
            MemoryAddress ocAnchor,
            MemoryAddress ocUri,
            MemorySegment ocTypes,
            int ocIfaceMask,
            MemoryAddress ocEndpoint,
            int ocBmMask) {
        var uri = toJavaStringRestricted(ocUri);

        logger.debug("OcDiscovery callback for uri: {}", uri);

        var anchor = toJavaStringRestricted(ocAnchor);
        var resourceTypes = OcHelpers.ocStringToList(ocTypes);
        var ifaceMask = ResourceInterfaces.fromInt(ocIfaceMask);

        // TODO: ocEndpoint is actually a list
       /*var eps = ocEndpoint.iterate(p -> !p.get().next$get().isNull())
               .map(ep -> OcEndpoint.fromNative(ep.get()))
               .collect(Collectors.toList());
       logger.debug("EPS => {}", eps);*/

        var endpoint = OcEndpoint.fromNative(ocEndpoint.address());

        var bmMask = ResourceProperties.fromInt(ocBmMask);

        return new Resource(anchor, uri, resourceTypes, ifaceMask, endpoint, bmMask);
    }

/*     private void handleSuccess(Consumer<AsyncResult<Resource>> handler, boolean success) {
        if (!success) {
            var errorMsg = "Failed to start discovery";
            logger.error(errorMsg);
            handler.accept(Future.failedFuture(errorMsg));
        }
    } */

    private void handleSuccess(Consumer<AsyncResult<Resource>> handler, byte success) {
        if (success == 0) {
            var errorMsg = "Failed to start discovery";
            logger.error(errorMsg);
            handler.accept(Future.failedFuture(errorMsg));
        }
    }
}
