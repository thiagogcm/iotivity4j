package tech.nthings.iotivity4j.binding.api.platform.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jdk.incubator.foreign.NativeScope;
import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.binding.api.platform.OcPlatformManager;
import tech.nthings.iotivity4j.platform.DeviceInfo;
import tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import static jdk.incubator.foreign.CLinker.*;
import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

public final class OcPlatformManagerImpl implements OcPlatformManager {
    private static final Logger logger = LoggerFactory.getLogger(OcPlatformManagerImpl.class);
    
    private static final Lock lock = new ReentrantLock();
    private static final Condition cv = lock.newCondition();

    private final NativeScope scope;
    private final AtomicBoolean hold;

    public OcPlatformManagerImpl() {
        this.scope = NativeScope.unboundedScope();
        this.hold = new AtomicBoolean(true);
    }

    @Override
    public void init(String name, List<DeviceInfo> devices, Consumer<AsyncResult<Void>> handler) {
        Objects.requireNonNull(name);

        //var ocHandler = scope.allocateStruct(oc_api_h.oc_handler_t.class);
        var ocHandler = oc_handler_t.allocate(scope);
        

        oc_handler_t.init$set(oc_init_platform$init_platform_cb.allocate(mainInitHandler(name, devices), scope), ocHandler.address());
        
        //ocHandler.init$set(scope.allocateCallback(mainInitHandler(name, devices)));
        //ocHandler.signal_event_loop$set(scope.allocateCallback(this::signalEventLoop));

        var res = oc_main_init(ocHandler);

        startMainPoll();

        if (res == 0) {
            handler.accept(Future.succeededFuture());
        } else {
            handler.accept(Future.failedFuture("Failed to init the platform")); // TODO: Collect errors from internalInitHandler
        }
    }

    @Override
    public void shutdown() {
        signalEventLoop();
        hold.set(false);
        oc_main_shutdown();
    }

    @Override
    public void cleanup() {
        scope.close();
    }

    private void startMainPoll() {
        new Thread(() -> {
            while (hold.get()) {
                var nextEvent = oc_main_poll();

                lock.lock();
                
                try {
                    if (nextEvent == 0) {
                        cv.await();
                    }
                } catch (InterruptedException e) {
                    logger.error("Failed to await lock condition", e);
                    Thread.currentThread().interrupt();
                } finally {
                    lock.unlock();
                }
            }
        }).start();
    }

    private oc_init_platform$init_platform_cb mainInitHandler(String name, List<DeviceInfo> devices) {
        return (var address) -> {
            logger.debug("MainInit callback!");

            
            var platformName = toCString(name, scope);

            var initRes = oc_init_platform(platformName, NULL(), NULL());
            
            //if (initRes != 0) {
            //	throw
            //}
            
            devices.forEach(d -> {
                var addDeviceRes = oc_add_device(
                        toCString(d.getUri(), scope),
                        toCString(d.getRt(), scope),
                        toCString(d.getName(), scope),
                        toCString(d.getSpecVersion(), scope),
                        toCString(d.getDataModelVersion(), scope),
                        NULL(),
                        NULL());

                //if (addDeviceRes != 0) {
                //	throw
                //}	
            });

            //return initRes;
            
        };
    }

    private void signalEventLoop() {
        logger.debug("SignalEventLoop!");

        lock.lock();

        try {
            cv.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
