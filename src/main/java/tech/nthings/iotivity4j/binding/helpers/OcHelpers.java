package tech.nthings.iotivity4j.binding.helpers;

import jdk.incubator.foreign.MemorySegment;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;

import tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;
import static tech.nthings.iotivity4j.binding.generated.org.iotivity.oc_api_h.*;

import static jdk.incubator.foreign.CLinker.*;

public interface OcHelpers {

	static List<String> ocStringToList(MemorySegment ocMmem) {
		return OcHelpers.ocStringToList(ocMmem, false);
	}

	static List<String> ocStringToList(MemorySegment ocMmem, boolean isByteArray) {

		/* TODO: Bug!: STRING_ARRAY_ITEM_MAX_LEN is 32 but should be 128 */
		//var size = ocMmem.size$get() / oc_helpers_lib.STRING_ARRAY_ITEM_MAX_LEN;




		
		var extraOffset = isByteArray ? 1 : 0;
		
		var result = new ArrayList<String>();

		var size = oc_mmem.size$get(ocMmem) / 128;
		//var size = ocMmem.size$get() / 128;
		//var rtPtr = ocMmem.ptr().get().ptr$get().cast(NativeTypes.CHAR);
		
		//for (var i = 0; i < size; i++) {
		//	rtPtr = rtPtr.offset(i * 128 + extraOffset);
		//	result.add(Pointer.toString(rtPtr));
		//}
		
		//return result;
		result.add(toJavaString(ocMmem));
		return result;
	}

/* 	static UUID ocUUIDToUUID(oc_uuid_t ocUuid) {
        var initArray = new byte[oc_uuid_lib.OC_UUID_LEN];

		try (var scope = oc_uuid_lib.scope().fork()) {
            var uuidStr = scope.allocateArray(NativeTypes.INT8, initArray);
            oc_uuid_lib.oc_uuid_to_str(ocUuid.ptr(), uuidStr.elementPointer(), oc_uuid_lib.OC_UUID_LEN);

			return UUID.fromString(Pointer.toString(uuidStr.elementPointer()));
        }
    }

	static oc_uuid_t UUIDToOcUUID(UUID uuid, Scope scope) {
		var ocUuid = scope.allocateStruct(oc_uuid_t.class);
		var nativeStr = scope.allocateCString(uuid.toString());

		oc_uuid_lib.oc_str_to_uuid(nativeStr, ocUuid.ptr());

		return ocUuid;
    } */

	static <E extends Enum<E>> int encode(EnumSet<E> set) {
		int ret = 0;

		for (E val : set) {
			ret |= 1 << val.ordinal();
		}

		return ret;
	}
}
