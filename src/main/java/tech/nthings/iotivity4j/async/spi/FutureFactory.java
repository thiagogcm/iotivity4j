package tech.nthings.iotivity4j.async.spi;

import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.async.Promise;

/**
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public interface FutureFactory {

    <T> Promise<T> promise();

    <T> Promise<T> succeededPromise();

    <T> Promise<T> succeededPromise(T result);

    <T> Promise<T> failedPromise(Throwable t);

    <T> Promise<T> failurePromise(String failureMessage);

    <T> Future<T> future();

    <T> Future<T> succeededFuture();

    <T> Future<T> succeededFuture(T result);

    <T> Future<T> failedFuture(Throwable t);

    <T> Future<T> failureFuture(String failureMessage);
}
