package tech.nthings.iotivity4j.async.impl;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.async.Promise;

import java.util.function.Consumer;

/**
 * @author <a href="mailto:julien@julienviet.com">Julien Viet</a>
 */
public class FailedFuture<T> implements Future<T>, Promise<T> {

    private final Throwable cause;

    /**
     * Create a future that has already failed
     * 
     * @param t the throwable
     */
    FailedFuture(Throwable t) {
        cause = t != null ? t : new Throwable(); // Fix this
    }

    /**
     * Create a future that has already failed
     * 
     * @param failureMessage the failure message
     */
    FailedFuture(String failureMessage) {
        this(new Throwable(failureMessage));
    }

    @Override
    public boolean isComplete() {
        return true;
    }

    @Override
    public Future<T> setHandler(Consumer<AsyncResult<T>> handler) {
        handler.accept(this);
        return this;
    }

    @Override
    public Consumer<AsyncResult<T>> getHandler() {
        return null;
    }

    @Override
    public void complete(T result) {
        throw new IllegalStateException("Result is already complete: failed");
    }

    @Override
    public void complete() {
        throw new IllegalStateException("Result is already complete: failed");
    }

    @Override
    public void fail(Throwable cause) {
        throw new IllegalStateException("Result is already complete: failed");
    }

    @Override
    public void fail(String failureMessage) {
        throw new IllegalStateException("Result is already complete: failed");
    }

    @Override
    public boolean tryComplete(T result) {
        return false;
    }

    @Override
    public boolean tryComplete() {
        return false;
    }

    @Override
    public boolean tryFail(Throwable cause) {
        return false;
    }

    @Override
    public boolean tryFail(String failureMessage) {
        return false;
    }

    @Override
    public T result() {
        return null;
    }

    @Override
    public Throwable cause() {
        return cause;
    }

    @Override
    public boolean succeeded() {
        return false;
    }

    @Override
    public boolean failed() {
        return true;
    }

    @Override
    public void accept(AsyncResult<T> asyncResult) {
        throw new IllegalStateException("Result is already complete: failed");
    }

    @Override
    public Future<T> future() {
        return this;
    }
}