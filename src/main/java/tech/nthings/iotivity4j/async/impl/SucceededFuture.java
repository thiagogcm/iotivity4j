package tech.nthings.iotivity4j.async.impl;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.async.Promise;

import java.util.function.Consumer;

/**
 * @author <a href="mailto:julien@julienviet.com">Julien Viet</a>
 */
class SucceededFuture<T> implements Future<T>, Promise<T> {

    private final T result;

    /**
     * Create a future that has already succeeded
     * 
     * @param result the result
     */
    SucceededFuture(T result) {
        this.result = result;
    }

    @Override
    public boolean isComplete() {
        return true;
    }

    @Override
    public Future<T> setHandler(Consumer<AsyncResult<T>> handler) {
        handler.accept(this);
        return this;
    }

    @Override
    public Consumer<AsyncResult<T>> getHandler() {
        return null;
    }

    @Override
    public void complete(T result) {
        throw new IllegalStateException("Result is already complete: succeeded");
    }

    @Override
    public void complete() {
        throw new IllegalStateException("Result is already complete: succeeded");
    }

    @Override
    public void fail(Throwable cause) {
        throw new IllegalStateException("Result is already complete: succeeded");
    }

    @Override
    public void fail(String failureMessage) {
        throw new IllegalStateException("Result is already complete: succeeded");
    }

    @Override
    public boolean tryComplete(T result) {
        return false;
    }

    @Override
    public boolean tryComplete() {
        return false;
    }

    @Override
    public boolean tryFail(Throwable cause) {
        return false;
    }

    @Override
    public boolean tryFail(String failureMessage) {
        return false;
    }

    @Override
    public T result() {
        return result;
    }

    @Override
    public Throwable cause() {
        return null;
    }

    @Override
    public boolean succeeded() {
        return true;
    }

    @Override
    public boolean failed() {
        return false;
    }

    @Override
    public void accept(AsyncResult<T> asyncResult) {
        throw new IllegalStateException("Result is already complete: succeeded");
    }

    @Override
    public Future<T> future() {
        return this;
    }

    @Override
    public String toString() {
        return result.toString();
    }
}
