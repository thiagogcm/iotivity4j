package tech.nthings.iotivity4j.async.impl;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.async.Promise;

import java.util.function.Consumer;

class FutureImpl<T> implements Promise<T>, Future<T> {

    private boolean failed;
    private boolean succeeded;
    private Consumer<AsyncResult<T>> handler;
    private T result;
    private Throwable throwable;

    /**
     * Create a future that hasn't completed yet
     */
    FutureImpl() {
    }

    /**
     * The result of the operation. This will be null if the operation failed.
     */
    public synchronized T result() {
        return result;
    }

    /**
     * An exception describing failure. This will be null if the operation
     * succeeded.
     */
    public synchronized Throwable cause() {
        return throwable;
    }

    /**
     * Did it succeeed?
     */
    public synchronized boolean succeeded() {
        return succeeded;
    }

    /**
     * Did it fail?
     */
    public synchronized boolean failed() {
        return failed;
    }

    /**
     * Has it completed?
     */
    public synchronized boolean isComplete() {
        return failed || succeeded;
    }

    /**
     * Set a handler for the result. It will get called when it's complete
     */
    public Future<T> setHandler(Consumer<AsyncResult<T>> handler) {
        boolean callHandler;
        synchronized (this) {
            callHandler = isComplete();
            if (!callHandler) {
                this.handler = handler;
            }
        }
        if (callHandler) {
            handler.accept(this);
        }
        return this;
    }

    @Override
    public synchronized Consumer<AsyncResult<T>> getHandler() {
        return handler;
    }

    @Override
    public void complete(T result) {
        if (!tryComplete(result)) {
            throw new IllegalStateException("Result is already complete: " + (succeeded ? "succeeded" : "failed"));
        }
    }

    @Override
    public void complete() {
        if (!tryComplete()) {
            throw new IllegalStateException("Result is already complete: " + (succeeded ? "succeeded" : "failed"));
        }
    }

    @Override
    public void fail(Throwable cause) {
        if (!tryFail(cause)) {
            throw new IllegalStateException("Result is already complete: " + (succeeded ? "succeeded" : "failed"));
        }
    }

    @Override
    public void fail(String failureMessage) {
        if (!tryFail(failureMessage)) {
            throw new IllegalStateException("Result is already complete: " + (succeeded ? "succeeded" : "failed"));
        }
    }

    @Override
    public boolean tryComplete(T result) {
        Consumer<AsyncResult<T>> h;
        synchronized (this) {
            if (succeeded || failed) {
                return false;
            }
            this.result = result;
            succeeded = true;
            h = handler;
            handler = null;
        }
        if (h != null) {
            h.accept(this);
        }
        return true;
    }

    @Override
    public boolean tryComplete() {
        return tryComplete(null);
    }

    public void accept(Future<T> ar) {
        if (ar.succeeded()) {
            complete(ar.result());
        } else {
            fail(ar.cause());
        }
    }

    @Override
    public void accept(AsyncResult<T> asyncResult) {
        if (asyncResult.succeeded()) {
            complete(asyncResult.result());
        } else {
            fail(asyncResult.cause());
        }
    }

    @Override
    public boolean tryFail(Throwable cause) {
        Consumer<AsyncResult<T>> h;
        synchronized (this) {
            if (succeeded || failed) {
                return false;
            }
            this.throwable = cause != null ? cause : new Throwable(); // TODO: Fix this
            failed = true;
            h = handler;
            handler = null;
        }
        if (h != null) {
            h.accept(this);
        }
        return true;
    }

    @Override
    public boolean tryFail(String failureMessage) {
        return tryFail(new Throwable(failureMessage));
    }

    @Override
    public Future<T> future() {
        return this;
    }
}
