package tech.nthings.iotivity4j.async.impl;

import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.async.Promise;
import tech.nthings.iotivity4j.async.spi.FutureFactory;

/**
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public class FutureFactoryImpl implements FutureFactory {

    private static final SucceededFuture EMPTY = new SucceededFuture<>(null);

    @Override
    public <T> Promise<T> promise() {
        return new FutureImpl<>();
    }

    @Override
    public <T> Promise<T> succeededPromise() {
        @SuppressWarnings("unchecked")
        Promise<T> promise = EMPTY;
        return promise;
    }

    @Override
    public <T> Promise<T> succeededPromise(T result) {
        return new SucceededFuture<>(result);
    }

    @Override
    public <T> Promise<T> failedPromise(Throwable t) {
        return new FailedFuture<>(t);
    }

    @Override
    public <T> Promise<T> failurePromise(String failureMessage) {
        return new FailedFuture<>(failureMessage);
    }

    @Override
    public <T> Future<T> future() {
        return new FutureImpl<>();
    }

    @Override
    public <T> Future<T> succeededFuture() {
        @SuppressWarnings("unchecked")
        Future<T> fut = EMPTY;
        return fut;
    }

    @Override
    public <T> Future<T> succeededFuture(T result) {
        return new SucceededFuture<>(result);
    }

    @Override
    public <T> Future<T> failedFuture(Throwable t) {
        return new FailedFuture<>(t);
    }

    @Override
    public <T> Future<T> failureFuture(String failureMessage) {
        return new FailedFuture<>(failureMessage);
    }
}
