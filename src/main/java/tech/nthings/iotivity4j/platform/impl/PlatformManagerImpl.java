package tech.nthings.iotivity4j.platform.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.async.Promise;
import tech.nthings.iotivity4j.binding.api.platform.OcPlatformManager;
import tech.nthings.iotivity4j.binding.api.platform.impl.OcPlatformManagerImpl;
import tech.nthings.iotivity4j.binding.spi.OcBinding;
import tech.nthings.iotivity4j.binding.storage.OcStorage;
import tech.nthings.iotivity4j.binding.storage.impl.OcStorageImpl;
import tech.nthings.iotivity4j.platform.DeviceInfo;
import tech.nthings.iotivity4j.platform.PlatformInfo;
import tech.nthings.iotivity4j.platform.PlatformManager;

import java.nio.file.Path;
import java.util.List;
import java.util.function.Consumer;

public class PlatformManagerImpl implements PlatformManager {
    private static final Logger logger = LoggerFactory.getLogger(PlatformManagerImpl.class);

    private final String name;
    private final Path path;
    private final List<DeviceInfo> devices;
    private final OcPlatformManager ocPlatformManager;
    private final OcStorage ocStorage;

    public PlatformManagerImpl(PlatformInfo platformInfo, List<DeviceInfo> devices) {
        //ocPlatformManager = OcBinding.of(OcPlatformManager.class);
        //ocStorage = OcBinding.of(OcStorage.class);
        ocPlatformManager = new OcPlatformManagerImpl();
        ocStorage = new OcStorageImpl();

        this.name = platformInfo.getName();
        this.path = platformInfo.getSvrPath();
        this.devices = devices;
    }

    @Override
    public void bootstrap(Consumer<AsyncResult<Void>> handler) {
    	if (devices.isEmpty()) {
    		throw new IllegalArgumentException("Cannot bootstrap platform without any device");
    	}

        var file = this.path.toFile();

        if (!file.exists() && !file.mkdirs()) {
            throw new IllegalArgumentException("Failed to create the Svr path");
        }

        this.ocStorage.config(path);
        this.ocPlatformManager.init(name, devices, handler);
    }

    @Override
    public Future<Void> bootstrap() {
        var promise = Promise.<Void>promise();
        bootstrap(promise);
        return promise.future();
    }

    @Override
    public void hold() {
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
        }
    }

    @Override
    public void shutdown() {
        logger.debug("Shutting down platform...");

        this.ocPlatformManager.shutdown();
        //OcBinding.cleanupAll();
    }

     public Thread defaultShutdownHook() {
         return new Thread(this::shutdown);
    }
}
