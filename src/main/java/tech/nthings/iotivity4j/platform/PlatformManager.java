package tech.nthings.iotivity4j.platform;

import tech.nthings.iotivity4j.async.AsyncResult;
import tech.nthings.iotivity4j.async.Future;
import tech.nthings.iotivity4j.platform.impl.PlatformManagerImpl;

import java.util.List;
import java.util.function.Consumer;

public interface PlatformManager {

    void bootstrap(Consumer<AsyncResult<Void>> handler);

    Future<Void> bootstrap();

    void hold();

    Thread defaultShutdownHook();

    void shutdown();

    static PlatformManager newPlatformManager(PlatformInfo platformInfo, List<DeviceInfo> devices) {
        return new PlatformManagerImpl(platformInfo, devices);
    }
}
