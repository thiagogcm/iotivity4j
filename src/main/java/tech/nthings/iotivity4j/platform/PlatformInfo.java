package tech.nthings.iotivity4j.platform;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;

public class PlatformInfo {
    private static final Logger logger = LoggerFactory.getLogger(PlatformInfo.class);

    private final String name;
    private final String pi;
    private final Path svrPath;

    public PlatformInfo(String name, String pi, Path svrPath) {
        this.name = name;
        this.pi = pi;
        this.svrPath = svrPath;
    }

    public PlatformInfo(String name, String pi) {
        this(name, pi, null);
    }

    public PlatformInfo(String name, Path svrPath) {
        this(name, null, svrPath);
    }

    public PlatformInfo(String name) {
        this(name, null, null);
    }

    public String getName() {
        return name;
    }

    public String getPi() {
        return pi;
    }

    public Path getSvrPath() {
        return svrPath;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert to JSON", e);
        }
        return null;
    }
}
