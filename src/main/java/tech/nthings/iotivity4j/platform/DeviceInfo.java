package tech.nthings.iotivity4j.platform;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceInfo {
    private static final Logger logger = LoggerFactory.getLogger(DeviceInfo.class);

    private final String name;
    private final String uri;
    private final String rt;
    private final String specVersion;
    private final String dataModelVersion;

    public DeviceInfo(String name, String uri, String rt, String specVersion, String dataModelVersion) {
        this.name = name;
        this.uri = uri;
        this.rt = rt;
        this.specVersion = specVersion;
        this.dataModelVersion = dataModelVersion;
    }

    public String getName() {
        return this.name;
    }

    public String getUri() {
        return this.uri;
    }

    public String getRt() {
        return this.rt;
    }

    public String getSpecVersion() {
        return this.specVersion;
    }

    public String getDataModelVersion() {
        return this.dataModelVersion;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert to JSON", e);
        }
        return null;
    }
}
