package tech.nthings.iotivity4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.nthings.iotivity4j.client.Client;
import tech.nthings.iotivity4j.client.ClientRequest;
import tech.nthings.iotivity4j.client.QualityOfService;
import tech.nthings.iotivity4j.discovery.Discovery;
import tech.nthings.iotivity4j.platform.DeviceInfo;
import tech.nthings.iotivity4j.platform.PlatformInfo;
import tech.nthings.iotivity4j.platform.PlatformManager;

import java.nio.file.Path;
import java.util.List;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        var nThingsDevice = new DeviceInfo("NThings Device", "/oic/d", "oic.d.phone", "ocf.2.0.0", "ocf.res.2.0.0");
        var nThingsPlatform = new PlatformInfo("NThings Platform", Path.of("./app_creds"));

        var platformManager = PlatformManager.newPlatformManager(nThingsPlatform, List.of(nThingsDevice));

        Runtime.getRuntime().addShutdownHook(platformManager.defaultShutdownHook());

        platformManager.bootstrap(br -> {
            if (br.succeeded()) {
                logger.info("Platform Initialized!!");

                var discovery = Discovery.newDiscovery();

                discovery.discoverAll(dr -> {
                    if (dr.succeeded()) {
                    	var discoveryResponse = dr.result();
                        logger.info("Discovery Response => " + dr);

/*                         var request = ClientRequest.newClientRequestBuilder()
                                .setMethod(ClientRequest.Method.GET)
                                .setQualityOfService(QualityOfService.LOW)
                                .setUri(discoveryResponse.getHref())
                                .setEndpoint(discoveryResponse.getEndpoint())
                                .build();
                        var client = Client.newClient();

                        client.send(request, cr -> {
                            if (cr.succeeded()) {
                                logger.info("GET Response => " + cr.result());
                            }
                        }); */
                    }
                });
            } else {
                logger.error("Platform Initialization Failed");
            }
        });

        platformManager.hold();
    }
}
