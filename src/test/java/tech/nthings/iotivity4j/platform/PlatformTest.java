package tech.nthings.iotivity4j.platform;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tech.nthings.iotivity4j.BaseTest;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DisplayName("Platform tests")
class PlatformTest extends BaseTest {

    @Test
    @DisplayName("Bootstrap")
    void testBootstrap() {
        var atomic = new AtomicBoolean(false);

        assertDoesNotThrow(() ->
                platformManager.bootstrap(ar ->
                        atomic.set(ar.succeeded())));

        await().untilTrue(atomic);
    }
}
