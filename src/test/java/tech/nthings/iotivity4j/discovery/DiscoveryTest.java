package tech.nthings.iotivity4j.discovery;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tech.nthings.iotivity4j.BaseTest;
import tech.nthings.iotivity4j.resource.Resource;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
class DiscoveryTest extends BaseTest {
    private Resource resource = null;

    @Test
    @DisplayName("Discover")
    void testDiscover() {
        var discovery = Discovery.newDiscovery();

        platformManager.bootstrap(br ->
                discovery.discover("core.light", dr ->
                        this.setResource(dr.result())));

        await().until(() -> this.resource != null);

        assertEquals("/a/light", this.resource.getHref());
    }

    @Test
    @DisplayName("Discover All")
    @Disabled
    void testDiscoverAll() {
        var discovery = Discovery.newDiscovery();

        platformManager.bootstrap(br ->
                discovery.discoverAll(dr ->
                        this.setResource(dr.result())));


        //assertEquals("/a/light", this.discoveryResponse.getUri());
    }

    private void setResource(Resource dr) {
        this.resource = dr;
    }
}
