package tech.nthings.iotivity4j;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import tech.nthings.iotivity4j.platform.DeviceInfo;
import tech.nthings.iotivity4j.platform.PlatformInfo;
import tech.nthings.iotivity4j.platform.PlatformManager;

import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class BaseTest {
    protected PlatformManager platformManager;

    @BeforeAll
    static void setup() {
        Awaitility.setDefaultTimeout(1, TimeUnit.SECONDS);
    }

    @BeforeEach
    void newPlatformManager() {
        var nThingsDevice = new DeviceInfo("NThings Device", "/oic/d", "oic.d.phone", "ocf.2.0.0", "ocf.res.2.0.0");
        var nThingsPlatform = new PlatformInfo("NThings Platform", Path.of("./app_creds"));
        platformManager = PlatformManager.newPlatformManager(nThingsPlatform, List.of(nThingsDevice));
    }

    @AfterEach
    void destroyPlatform() {
        platformManager.shutdown();
    }
}
